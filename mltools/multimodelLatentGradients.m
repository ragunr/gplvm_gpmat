function gX = multimodelLatentGradients(model);

% GPDYNAMICSLATENTGRADIENTS Gradients of the X vector given the dynamics model.
% FORMAT
% DESC Compute the gradients of the log likelihood of a Gaussian 
% proces dynamics model with respect to the latent points in the
% GP-LVM model.
% ARG model : the GP model for which log likelihood is to be
% computed.
% RETURN gX : the gradients with respec to latent points.
%
% SEEALSO : gpDynamicsCreate, gpDynamicsLogLikelihood
%
% COPYRIGHT : Neil D. Lawrence, 2006, 2009
%
% MODIFICATIONS : Carl Henrik Ek, 2006
  
% FGPLVM

gX = modelLatentGradients(model.comp{1});
for i = 2:model.numModels
    gX = gX + modelLatentGradients(model.comp{i});
end

