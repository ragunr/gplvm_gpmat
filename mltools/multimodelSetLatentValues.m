function model = multimodelSetLatentValues(model, X)

% GPDYNAMICSSETLATENTVALUES Set the latent values inside the model.
% FORMAT
% DESC Distributes GP-LVM latent positions throughout the GP dynamics model. 
% ARG model : the model in which the latent positions are to be
% placed.
% ARG X : the latent positions to be placed in the model.
% RETURN model : the updated model with the relevant latent
% positions in place.
%
% SEEALSO : gpDynamicsCreate, gpDynamicsLogLikelihood
%
% COPYRIGHT : Neil D. Lawrence and Carl Henrik Ek, 2006
%
% MODIFICATIONS : Carl Henrik Ek, 2008

for i = 1:model.numModels
    model.comp{i} = modelSetLatentValues(model.comp{i}, X);
end

